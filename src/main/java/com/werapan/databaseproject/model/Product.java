/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.model;

import com.werapan.databaseproject.dao.CustomerDao;
import com.werapan.databaseproject.dao.UserDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Asus
 */
public class Product {
    private int productId;
    private String productName;
    private float productPrice;
    private String productSize;
    private String productSweetLevel;
    private String productType;
    private int categoryId;

    public Product(int productId, String productName, float productPrice, String productSize, String productSweetLevel, String productType, int categoryId) {
        this.productId = productId;
        this.productName = productName;
        this.productPrice = productPrice;
        this.productSize = productSize;
        this.productSweetLevel = productSweetLevel;
        this.productType = productType;
        this.categoryId = categoryId;
    }
    
     public Product(String productName, float productPrice, String productSize, String productSweetLevel, String productType, int categoryId) {
        this.productId = -1;
        this.productName = productName;
        this.productPrice = productPrice;
        this.productSize = productSize;
        this.productSweetLevel = productSweetLevel;
        this.productType = productType;
        this.categoryId = categoryId;
    }
     
     public Product() {
        this.productId = -1;
        this.productName = "";
        this.productPrice = 0;
        this.productSize = "";
        this.productSweetLevel = "";
        this.productType = "";
        this.categoryId = 0;
    }

    public int getId() {
        return productId;
    }

    public void setId(int productId) {
        this.productId = productId;
    }

    public String getName() {
        return productName;
    }

    public void setName(String productName) {
        this.productName = productName;
    }

    public float getPrice() {
        return productPrice;
    }

    public void setPrice(float productPrice) {
        this.productPrice = productPrice;
    }

    public String getSize() {
        return productSize;
    }

    public void setSize(String productSize) {
        this.productSize = productSize;
    }

    public String getSweetLevel() {
        return productSweetLevel;
    }

    public void setSweetLevel(String productSweetLevel) {
        this.productSweetLevel = productSweetLevel;
    }

    public String getType() {
        return productType;
    }

    public void setType(String productType) {
        this.productType = productType;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public String toString() {
        return "Product{" + "productId=" + productId + ", productName=" + productName + ", productPrice=" + productPrice + ", productSize=" + productSize + ", productSweetLevel=" + productSweetLevel + ", productType=" + productType + ", categoryId=" + categoryId + '}';
    }
     
    public static Product fromRS(ResultSet rs) {
        Product product = new Product();
        try {
            product.setId(rs.getInt("product_id"));
            product.setName(rs.getString("product_name"));
            product.setPrice(rs.getFloat("product_price"));
            product.setSize(rs.getString("product_size"));
            product.setSweetLevel(rs.getString("product_sweet_level"));
            product.setType(rs.getString("product_type"));
            product.setCategoryId(rs.getInt("category_id"));
            
        } catch (SQLException ex) {
            Logger.getLogger(Product.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return product;
    }
}
